# FileCheckSum

A simple project that performs the following:
1. Compute SHA256 hash for each file located in a folder path. Store to a JSON file structure.
2. Compare the hash of two objects for equality.

You can use this project to check files for tampering.